/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "player.h"

#include "playercontrols.h"
#include "playlistmodel.h"
#include "the_button.h"
#include "videowidget.h"

#include <QMediaService>
#include <QMediaPlaylist>
#include <QVideoProbe>
#include <QAudioProbe>
#include <QMediaMetaData>
#include <QtWidgets>

Player::Player(std::vector<QString> videoName, std::vector<TheButtonInfo> videos, QWidget *parent)
    : QWidget(parent)
{
    this->setStyleSheet("background-color:white");
    this->videoName = videoName;
    this->videos = videos;
    //! [create-objs]
    m_player = new QMediaPlayer(this);
    m_player->setAudioRole(QAudio::VideoRole);
    qInfo() << "Supported audio roles:";
    for (QAudio::Role role : m_player->supportedAudioRoles())
        qInfo() << "    " << role;
    // owned by PlaylistModel
    m_playlist = new QMediaPlaylist();
    m_player->setPlaylist(m_playlist);
    //! [create-objs]

    connect(m_player, &QMediaPlayer::durationChanged, this, &Player::durationChanged);
    connect(m_player, &QMediaPlayer::positionChanged, this, &Player::positionChanged);
    connect(m_player, QOverload<>::of(&QMediaPlayer::metaDataChanged), this, &Player::metaDataChanged);
    connect(m_playlist, &QMediaPlaylist::currentIndexChanged, this, &Player::playlistPositionChanged);
    connect(m_player, &QMediaPlayer::mediaStatusChanged, this, &Player::statusChanged);
    connect(m_player, &QMediaPlayer::bufferStatusChanged, this, &Player::bufferingProgress);
    connect(m_player, &QMediaPlayer::videoAvailableChanged, this, &Player::videoAvailableChanged);
    connect(m_player, QOverload<QMediaPlayer::Error>::of(&QMediaPlayer::error), this, &Player::displayErrorMessage);
    //connect(m_player, &QMediaPlayer::stateChanged, this, &Player::stateChanged);

    //! [2]
    m_videoWidget = new VideoWidget(this);
    m_player->setVideoOutput(m_videoWidget);

    m_playlistModel = new PlaylistModel(this);
    m_playlistModel->setPlaylist(m_playlist);
    //! [2]

    m_playlistView = new QListView(this);
    m_playlistView->setModel(m_playlistModel);
    m_playlistView->setGeometry(QRect(-50, -50, 1, 1));

    // create scrollArea
    QScrollArea *scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);

    scrollArea->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // a row of buttons
    QWidget *buttonWidget = new QWidget();
    // a list of the buttons
    std::vector<TheButton*> buttons;
    // the buttons are arranged vertically
    QVBoxLayout *bttnlayout = new QVBoxLayout();

    buttonWidget->setLayout(bttnlayout);
    scrollArea->setWidget(buttonWidget);

    // Sets the minimum width of the scroll area
    scrollArea->setMinimumWidth(300);
    scrollArea->setStyleSheet("QScrollBar:vertical"
                              " {"
                              "     background-color: white;    "
                              "border:solid 1px;"
                              "width:15px;"
                              " }"
                              ""
                              " QScrollBar::handle:vertical"
                              " {"
                              "     background-color: #164E87;   "
                              "     border-radius: 5px;"
                              " }"
                              "QScrollArea{border:none;}"

                              );

    // create the four buttons and set their icons as thumbnails to the videos with the video names above them
    for ( int i = 0; i < (int)videos.size(); i++ ) {
        TheButton *bttn = new TheButton(buttonWidget);
        // when clicked, tell the player to play.
        connect(bttn, SIGNAL(jumpTo(TheButtonInfo* )), this, SLOT (jumpTo(TheButtonInfo* )));

        buttons.push_back(bttn);

        m_videoName = new QLabel();
        m_videoName->setText(videoName.at(i));

        bttnlayout->addWidget(m_videoName);
        bttnlayout->addWidget(bttn);

        // adds the images on the buttons
        bttn->init(&videos.at(i));
    }

    m_playlistView->setCurrentIndex(m_playlistModel->index(m_playlist->currentIndex(), 0));

    connect(m_playlistView, &QAbstractItemView::activated, this, &Player::jump);

    m_slider = new QSlider(Qt::Horizontal, this);
    m_slider->setRange(0, m_player->duration() / 1000);
    m_slider->setStyleSheet("QSlider::groove:horizontal { "
                            "	background-color: black;"
                            "	border: 1px solid black; "
                            "	height: 1px; "
                            "}"
                            ""
                            "QSlider::handle:horizontal { "
                            "	background-color: #164E87; "
                            "	border: 2px solid #164E87; "
                            "	width: 16px; "
                            "	height: 10px; "
                            "	line-height: 10px; "
                            "	margin-top: -10px; "
                            "	margin-bottom: -10px; "
                            "	border-radius: 10px; "
                            "}"
                            );
    m_labelDuration = new QLabel(this);
    m_labelDuration->setStyleSheet("color:#164E87;");
    connect(m_slider, &QSlider::sliderMoved, this, &Player::seek);


    PlayerControls *controls = new PlayerControls(this);
    controls->setState(m_player->state());
    controls->setVolume(m_player->volume());
    controls->setMuted(controls->isMuted());

    connect(controls, &PlayerControls::play, m_player, &QMediaPlayer::play);
    connect(controls, &PlayerControls::pause, m_player, &QMediaPlayer::pause);
    connect(controls, &PlayerControls::stop, m_player, &QMediaPlayer::stop);
    connect(controls, &PlayerControls::next, m_playlist, &QMediaPlaylist::next);
    connect(controls, &PlayerControls::previous, this, &Player::previousClicked);
    connect(controls, &PlayerControls::changeVolume, m_player, &QMediaPlayer::setVolume);
    connect(controls, &PlayerControls::changeMuting, m_player, &QMediaPlayer::setMuted);
    connect(controls, &PlayerControls::changeRate, m_player, &QMediaPlayer::setPlaybackRate);
    connect(controls, &PlayerControls::stop, m_videoWidget, QOverload<>::of(&QVideoWidget::update));

    connect(m_player, &QMediaPlayer::stateChanged, controls, &PlayerControls::setState);
    connect(m_player, &QMediaPlayer::volumeChanged, controls, &PlayerControls::setVolume);
    connect(m_player, &QMediaPlayer::mutedChanged, controls, &PlayerControls::setMuted);

    m_fullScreenButton = new QPushButton();
    m_fullScreenButton->setIcon(QIcon(":/src/doc/images/fullscreen.xpm"));
    m_fullScreenButton->setCheckable(true);
    m_fullScreenButton->setStyleSheet("margin:0px;border:none;");



    m_colorButton = new QPushButton(tr("  color options  "), this);//Color Options...
    m_colorButton->setEnabled(false);
    m_colorButton->setStyleSheet("QPushButton { /* all types of tool button */"
                                 "    padding:10px;"
                                 "    background-color:#164E87;"
                                 "    border: 2px solid #164E87;"
                                 "    border-radius: 26px;"
                                 "    color:white;"
                                 "}");

    connect(m_colorButton, &QPushButton::clicked, this, &Player::showColorDialog);

    m_videoName = new QLabel();
    m_videoName->setText(videoName.at(0));
    //Logo
    QPushButton *m_logo = new QPushButton(this);


    m_logo->setStyleSheet("margin:0px;background-image:url(':/src/doc/images/tomeo.xpm');"
                          "background-repeat: no-repeat;height:100%;width:190%;border:none;");

    //Search


    search = new QLineEdit();
    search->setPlaceholderText(" Enter Video name");
    search->setStyleSheet("QLineEdit{"
                          "background-color:white;"
                          "border: 4px solid #164E87;"
                         " border-radius: 16px;"
                          "color:#164E87;"
                          "}");
    searchButton = new QPushButton(" search ", this);
    searchButton->setStyleSheet("QPushButton { /* all types of tool button */"
                                "    padding:10px;"
                                "    background-color:#164E87;"
                                "    border: 2px solid #164E87;"
                                "    border-radius: 26px;"
                                "    color:white;"
                                "}");
    connect(searchButton, &QPushButton::clicked, this, &Player::searchResults);

    QBoxLayout *searchLayout = new QHBoxLayout;
    searchLayout->addWidget(m_logo);
    searchLayout->addSpacing(50);
    searchLayout->addWidget(search);
    searchLayout->addWidget(searchButton);


    QBoxLayout *videoLayout = new QVBoxLayout;
    videoLayout->addLayout(searchLayout);
    videoLayout->addWidget(m_videoName);


    QBoxLayout *displayLayout = new QHBoxLayout;
    displayLayout->addWidget(m_videoWidget, 2);
    displayLayout->addWidget(scrollArea);

    videoLayout->addLayout(displayLayout);
    QBoxLayout *controlLayout = new QHBoxLayout;
    controlLayout->setContentsMargins(10, 0, 0, 0);
    controlLayout->addWidget(controls);
    controlLayout->addWidget(m_colorButton);
    controlLayout->addWidget(m_fullScreenButton);


    QBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(videoLayout);
    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addWidget(m_slider);
    hLayout->addWidget(m_labelDuration);


    layout->addLayout(hLayout);

    layout->addLayout(controlLayout);
    layout->setContentsMargins(50,25,50,25);
    //    layout->addLayout(histogramLayout);
#if defined(Q_OS_QNX)
    // On QNX, the main window doesn't have a title bar (or any other decorations).
    // Create a status bar for the status information instead.
    m_statusLabel = new QLabel;
    m_statusBar = new QStatusBar;
    m_statusBar->addPermanentWidget(m_statusLabel);
    m_statusBar->setSizeGripEnabled(false); // Without mouse grabbing, it doesn't work very well.
    layout->addWidget(m_statusBar);
#endif

    setLayout(layout);

    if (!isPlayerAvailable()) {
        QMessageBox::warning(this, tr("Service not available"),
                             tr("The QMediaPlayer object does not have a valid service.\n"\
                                "Please check the media service plugins are installed."));

        controls->setEnabled(false);
        m_playlistView->setEnabled(false);
        //openButton->setEnabled(false);
        m_colorButton->setEnabled(false);
        m_fullScreenButton->setEnabled(false);
    }

    metaDataChanged();
}

Player::~Player()
{
}

// plays the video on the media player with the help of the url
void Player::jumpTo (TheButtonInfo* button) {
    m_player->setMedia( * button -> url);
    m_player->play();
}

bool Player::isPlayerAvailable() const
{
    return m_player->isAvailable();
}

void Player::open()
{
    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setWindowTitle(tr("Open Files"));
    QStringList supportedMimeTypes = m_player->supportedMimeTypes();
    if (!supportedMimeTypes.isEmpty()) {
        supportedMimeTypes.append("audio/x-m3u"); // MP3 playlists
        fileDialog.setMimeTypeFilters(supportedMimeTypes);
    }
    fileDialog.setDirectory(QStandardPaths::standardLocations(QStandardPaths::MoviesLocation).value(0, QDir::homePath()));
    if (fileDialog.exec() == QDialog::Accepted)
        addToPlaylist(fileDialog.selectedUrls());
}

static bool isPlaylist(const QUrl &url) // Check for ".m3u" playlists.
{
    if (!url.isLocalFile())
        return false;
    const QFileInfo fileInfo(url.toLocalFile());
    return fileInfo.exists() && !fileInfo.suffix().compare(QLatin1String("m3u"), Qt::CaseInsensitive);
}

void Player::addToPlaylist(const QList<QUrl> &urls)
{
    for (auto &url: urls) {
        if (isPlaylist(url))
            m_playlist->load(url);
        else
            m_playlist->addMedia(url);
    }
}

void Player::setCustomAudioRole(const QString &role)
{
    m_player->setCustomAudioRole(role);
}

void Player::durationChanged(qint64 duration)
{
    m_duration = duration / 1000;
    m_slider->setMaximum(m_duration);
}

void Player::positionChanged(qint64 progress)
{
    if (!m_slider->isSliderDown())
        m_slider->setValue(progress / 1000);

    updateDurationInfo(progress / 1000);
}

void Player::metaDataChanged()
{
    if (m_player->isMetaDataAvailable()) {
        setTrackInfo(QString("%1 - %2")
                     .arg(m_player->metaData(QMediaMetaData::AlbumArtist).toString())
                     .arg(m_player->metaData(QMediaMetaData::Title).toString()));

        if (m_coverLabel) {
            QUrl url = m_player->metaData(QMediaMetaData::CoverArtUrlLarge).value<QUrl>();

            m_coverLabel->setPixmap(!url.isEmpty()
                                    ? QPixmap(url.toString())
                                    : QPixmap());
        }
    }
}

void Player::previousClicked()
{
    // Go to previous track if we are within the first 5 seconds of playback
    // Otherwise, seek to the beginning.
    if (m_player->position() <= 5000)
        m_playlist->previous();
    else
        m_player->setPosition(0);
}

void Player::jump(const QModelIndex &index)
{
    if (index.isValid()) {
        m_playlist->setCurrentIndex(index.row());
        m_player->play();
    }
}

void Player::playlistPositionChanged(int currentItem)
{
    //    clearHistogram();
    m_playlistView->setCurrentIndex(m_playlistModel->index(currentItem, 0));
}

void Player::seek(int seconds)
{
    m_player->setPosition(seconds * 1000);
}

void Player::statusChanged(QMediaPlayer::MediaStatus status)
{
    handleCursor(status);

    // handle status message
    switch (status) {
    case QMediaPlayer::UnknownMediaStatus:
    case QMediaPlayer::NoMedia:
    case QMediaPlayer::LoadedMedia:
        setStatusInfo(QString());
        break;
    case QMediaPlayer::LoadingMedia:
        setStatusInfo(tr("Loading..."));
        break;
    case QMediaPlayer::BufferingMedia:
    case QMediaPlayer::BufferedMedia:
        setStatusInfo(tr("Buffering %1%").arg(m_player->bufferStatus()));
        break;
    case QMediaPlayer::StalledMedia:
        setStatusInfo(tr("Stalled %1%").arg(m_player->bufferStatus()));
        break;
    case QMediaPlayer::EndOfMedia:
        QApplication::alert(this);
        break;
    case QMediaPlayer::InvalidMedia:
        displayErrorMessage();
        break;
    }
}

//void Player::stateChanged(QMediaPlayer::State state)
//{
////    if (state == QMediaPlayer::StoppedState)
////        clearHistogram();
//}

void Player::handleCursor(QMediaPlayer::MediaStatus status)
{
#ifndef QT_NO_CURSOR
    if (status == QMediaPlayer::LoadingMedia ||
            status == QMediaPlayer::BufferingMedia ||
            status == QMediaPlayer::StalledMedia)
        setCursor(QCursor(Qt::BusyCursor));
    else
        unsetCursor();
#endif
}

void Player::bufferingProgress(int progress)
{
    if (m_player->mediaStatus() == QMediaPlayer::StalledMedia)
        setStatusInfo(tr("Stalled %1%").arg(progress));
    else
        setStatusInfo(tr("Buffering %1%").arg(progress));
}

void Player::videoAvailableChanged(bool available)
{
    if (!available) {
        disconnect(m_fullScreenButton, &QPushButton::clicked, m_videoWidget, &QVideoWidget::setFullScreen);
        disconnect(m_videoWidget, &QVideoWidget::fullScreenChanged, m_fullScreenButton, &QPushButton::setChecked);
        m_videoWidget->setFullScreen(false);
    } else {
        connect(m_fullScreenButton, &QPushButton::clicked, m_videoWidget, &QVideoWidget::setFullScreen);
        connect(m_videoWidget, &QVideoWidget::fullScreenChanged, m_fullScreenButton, &QPushButton::setChecked);

        if (m_fullScreenButton->isChecked())
            m_videoWidget->setFullScreen(true);
    }
    m_colorButton->setEnabled(available);
}

void Player::setTrackInfo(const QString &info)
{
    m_trackInfo = info;

    if (m_statusBar) {
        m_statusBar->showMessage(m_trackInfo);
        m_statusLabel->setText(m_statusInfo);
    } else {
        setWindowTitle("Tomeo");
    }
}

void Player::setStatusInfo(const QString &info)
{
    m_statusInfo = info;

    if (m_statusBar) {
        m_statusBar->showMessage(m_trackInfo);
        m_statusLabel->setText(m_statusInfo);
    } else {
        setWindowTitle("Tomeo");
    }
}

void Player::displayErrorMessage()
{
    setStatusInfo(m_player->errorString());
}

void Player::updateDurationInfo(qint64 currentInfo)
{
    QString tStr;
    if (currentInfo || m_duration) {
        QTime currentTime((currentInfo / 3600) % 60, (currentInfo / 60) % 60,
                          currentInfo % 60, (currentInfo * 1000) % 1000);
        QTime totalTime((m_duration / 3600) % 60, (m_duration / 60) % 60,
                        m_duration % 60, (m_duration * 1000) % 1000);
        QString format = "mm:ss";
        if (m_duration > 3600)
            format = "hh:mm:ss";
        tStr = currentTime.toString(format) + " / " + totalTime.toString(format);
    }
    m_labelDuration->setText(tStr);
}

void Player::showColorDialog()
{
    if (!m_colorDialog) {
        QSlider *brightnessSlider = new QSlider(Qt::Horizontal);
        brightnessSlider->setRange(-100, 100);
        brightnessSlider->setStyleSheet("QSlider::groove:horizontal { "
                                        "	background-color: black;"
                                        "	border: 1px solid black; "
                                        "	height: 1px; "
                                        "}"
                                        ""
                                        "QSlider::handle:horizontal { "
                                        "	background-color: #164E87; "
                                        "	border: 2px solid #164E87; "
                                        "	width: 16px; "
                                        "	height: 10px; "
                                        "	line-height: 10px; "
                                        "	margin-top: -10px; "
                                        "	margin-bottom: -10px; "
                                        "	border-radius: 10px; "
                                        "}"
                                        );
        brightnessSlider->setValue(m_videoWidget->brightness());
        connect(brightnessSlider, &QSlider::sliderMoved, m_videoWidget, &QVideoWidget::setBrightness);
        connect(m_videoWidget, &QVideoWidget::brightnessChanged, brightnessSlider, &QSlider::setValue);

        QSlider *contrastSlider = new QSlider(Qt::Horizontal);
        contrastSlider->setRange(-100, 100);
        contrastSlider->setValue(m_videoWidget->contrast());
        contrastSlider->setStyleSheet("QSlider::groove:horizontal { "
                                      "	background-color: black;"
                                      "	border: 1px solid black; "
                                      "	height: 1px; "
                                      "}"
                                      ""
                                      "QSlider::handle:horizontal { "
                                      "	background-color: #164E87; "
                                      "	border: 2px solid #164E87; "
                                      "	width: 16px; "
                                      "	height: 10px; "
                                      "	line-height: 10px; "
                                      "	margin-top: -10px; "
                                      "	margin-bottom: -10px; "
                                      "	border-radius: 10px; "
                                      "}"
                                      );
        connect(contrastSlider, &QSlider::sliderMoved, m_videoWidget, &QVideoWidget::setContrast);
        connect(m_videoWidget, &QVideoWidget::contrastChanged, contrastSlider, &QSlider::setValue);

        QSlider *hueSlider = new QSlider(Qt::Horizontal);
        hueSlider->setRange(-100, 100);
        hueSlider->setValue(m_videoWidget->hue());
        hueSlider->setStyleSheet("QSlider::groove:horizontal { "
                                 "	background-color: black;"
                                 "	border: 1px solid black; "
                                 "	height: 1px; "
                                 "}"
                                 ""
                                 "QSlider::handle:horizontal { "
                                 "	background-color: #164E87; "
                                 "	border: 2px solid #164E87; "
                                 "	width: 16px; "
                                 "	height: 10px; "
                                 "	line-height: 10px; "
                                 "	margin-top: -10px; "
                                 "	margin-bottom: -10px; "
                                 "	border-radius: 10px; "
                                 "}"
                                 );
        connect(hueSlider, &QSlider::sliderMoved, m_videoWidget, &QVideoWidget::setHue);
        connect(m_videoWidget, &QVideoWidget::hueChanged, hueSlider, &QSlider::setValue);

        QSlider *saturationSlider = new QSlider(Qt::Horizontal);
        saturationSlider->setRange(-100, 100);
        saturationSlider->setStyleSheet("QSlider::groove:horizontal { "
                                        "	background-color: black;"
                                        "	border: 1px solid black; "
                                        "	height: 1px; "
                                        "}"
                                        ""
                                        "QSlider::handle:horizontal { "
                                        "	background-color: #164E87; "
                                        "	border: 2px solid #164E87; "
                                        "	width: 16px; "
                                        "	height: 10px; "
                                        "	line-height: 10px; "
                                        "	margin-top: -10px; "
                                        "	margin-bottom: -10px; "
                                        "	border-radius: 10px; "
                                        "}"
                                        );
        saturationSlider->setValue(m_videoWidget->saturation());
        connect(saturationSlider, &QSlider::sliderMoved, m_videoWidget, &QVideoWidget::setSaturation);
        connect(m_videoWidget, &QVideoWidget::saturationChanged, saturationSlider, &QSlider::setValue);

        QFormLayout *layout = new QFormLayout;
        QLabel *brightness=new QLabel(tr("Brightness"));
        brightness->setStyleSheet("color:#164E87;");
        QLabel *contarst=new QLabel(tr("Contrast"));
        contarst->setStyleSheet("color:#164E87;");
        QLabel *hue=new QLabel(tr("Hue"));
        hue->setStyleSheet("color:#164E87;");
        QLabel *saturation=new QLabel(tr("Saturation"));
        saturation->setStyleSheet("color:#164E87;");

        layout->addRow(brightness, brightnessSlider);
        layout->addRow(contarst, contrastSlider);
        layout->addRow(hue, hueSlider);
        layout->addRow(saturation, saturationSlider);

        QPushButton *button = new QPushButton(tr("Close"));
        layout->addRow(button);
        button->setStyleSheet("QPushButton { /* all types of tool button */"
                              "    padding:10px;"
                              "    background-color:#164E87;"
                              "    border: 2px solid #164E87;"
                              "    border-radius: 26px;"
                              "    color:white;"
                              "}");

        m_colorDialog = new QDialog(this);
        m_colorDialog->setStyleSheet("color:white;");
        m_colorDialog->setWindowTitle(tr("Color Options"));
        m_colorDialog->setLayout(layout);

        connect(button, &QPushButton::clicked, m_colorDialog, &QDialog::close);
    }
    m_colorDialog->show();
}
// stores all the video names with the searched text in vector of QString.
std::vector<QString> Player::search_for_string(std::vector<QString> videoName,QString search_str) {
    std::vector<QString> my_found_items;

    for( int i = 0; i < (int)videoName.size(); i++ ) {
        //checks if the video name contains the string
        if( videoName.at(i).contains( search_str ) ) {
            my_found_items.push_back(videoName.at(i));
        }
    }
    return my_found_items;
}

void Player::searchResults() {
    QDialog *dialog = new QDialog(this);
    // stores all the video names with the searched text in vector of QString called searchNames.
    std::vector<QString> searchNames = search_for_string( videoName, search->text() );

    QWidget *buttonWidget = new QWidget();
    QWidget *scrollWidget = new QWidget();

    QGridLayout *gridLayout = new QGridLayout(this);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *filterLayout = new QHBoxLayout();

    // Adds consistent spacong between the buttons
    gridLayout->addItem(new QSpacerItem(1,1,QSizePolicy::Preferred,QSizePolicy::Expanding),6+2,1);
    gridLayout->addItem(new QSpacerItem(1,1,QSizePolicy::Preferred,QSizePolicy::Expanding),1,0);
    gridLayout->addItem(new QSpacerItem(1,1,QSizePolicy::Preferred,QSizePolicy::Expanding),1,6+2);

    // adds scroll area
    QScrollArea *ScrollArea = new QScrollArea();
    ScrollArea->setWidgetResizable(true);
    ScrollArea->setStyleSheet("border:none");

    // create filter widgets for date and video length
    QPushButton *filterIcon = new QPushButton();
    filterIcon->setIcon(QIcon(":/src/doc/images/filter.xpm"));
    filterIcon->setStyleSheet("border:none");
    QPushButton *dateFilter = new QPushButton("Sort by date");
    dateFilter->setStyleSheet("QPushButton { /* all types of tool button */"
                                 "    padding:10px;"
                                 "    background-color:#164E87;"
                                 "    border: 2px solid #164E87;"
                                 "    border-radius: 20px;"
                                 "    color:white;"
                                 "}");
    QPushButton *lengthFilter = new QPushButton("Sort by length");
    lengthFilter->setStyleSheet("QPushButton { /* all types of tool button */"
                                 "    padding:10px;"
                                 "    background-color:#164E87;"
                                 "    border: 2px solid #164E87;"
                                 "    border-radius: 20px;"
                                 "    color:white;"
                                 "}");
    QPushButton *createMap = new QPushButton("Create a map");
    createMap->setStyleSheet("QPushButton { /* all types of tool button */"
                                 "    padding:10px;"
                                 "    background-color:#164E87;"
                                 "    border: 2px solid #164E87;"
                                 "    border-radius: 20px;"
                                 "    color:white;"
                                 "}");
    dateFilter->setMaximumSize(QSize(200, 50));
    lengthFilter->setMaximumSize(QSize(200, 50));
    createMap->setMaximumSize(QSize(200, 50));

    ScrollArea->setWidget(scrollWidget);
    scrollWidget->setLayout(gridLayout);

    // a list of the buttons
    std::vector<TheButton*> buttons;

    int rowCount = 0;
    int colCount = 0;

    // create the four buttons and set their icons as thumbnails to the videos with the video
    // names above them
    for ( int i = 0; i < (int)videos.size(); i++ ) {
        for ( int j = 0; j < (int)searchNames.size(); j++ ) {
            //returns 0 if strings of video name and searched text same
            int x = QString::compare(searchNames.at(j), videoName.at(i), Qt::CaseInsensitive);
            if(x == 0) {
                QVBoxLayout *buttonNameLayout = new QVBoxLayout;
                QWidget *bttnWidget = new QWidget();

                TheButton *bttn = new TheButton(buttonWidget);
                // when clicked, tell the player to play.
                bttn->connect(bttn, SIGNAL(jumpTo(TheButtonInfo* )), this,
                              SLOT (jumpTo(TheButtonInfo*)));
                buttons.push_back(bttn);

                QLabel *m_videoNames = new QLabel();
                m_videoNames->setText(videoName.at(i));
                bttn->setFixedSize(QSize(200,110));

                buttonNameLayout->addWidget(m_videoNames);
                buttonNameLayout->addWidget(bttn);
                bttnWidget->setLayout(buttonNameLayout);

                // goes to next row if 4 buttons in one row and resets column count
                if (j%4 == 0) {
                    rowCount++;
                    colCount = 0;
                }
                colCount++;

                // adds the buttons in a grid layout with 4 columns maximum
                gridLayout->addWidget(bttnWidget, rowCount, colCount);

                // adds the images on the buttons
                bttn->init(&videos.at(i));
            }
        }
    }
    filterLayout->addWidget(filterIcon);
    filterLayout->addWidget(dateFilter);
    filterLayout->addWidget(lengthFilter);
    filterLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    mainLayout->addLayout(filterLayout);
    mainLayout->addWidget(ScrollArea);
    mainLayout->addWidget(createMap);

    dialog->setLayout(mainLayout);
    dialog->show();
}
